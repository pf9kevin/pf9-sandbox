var request = require('request');
var mail = require("./mail.js");
var Models = require("./model.js");
var User = Models.User;

var OPENSTACK_USERNAME = "klee@platform9.com";
var OPENSTACK_PASSWORD = "1Password!";
var SERVICE_TENANT_ID = "2a83c04cc091442393fa4c2cb244c89f";
var ADMIN_ROLE_ID = "21eae6a5220740f5ab6c03c48473e39f";

// User.remove({}, function(err, users) {
//     console.log(users);
// });

User.find({}, function(err, users) {
    console.log(users);
});

var updateUser = function(username, keystoneId, tenantId, cb) {
    User.findById(username, function(err, user) {
        user.keystone_id = keystoneId;
        user.tenant_id = tenantId;
        user.save(function(err) {
            console.log("saved user");
            console.log(user);
            if (cb) {
                cb(user);
            }
        });
    });
};

var Openstack = {
    getUnscopedToken: function(cb) {
        var requestOptions = {
            url: "https://klee-multi-glance.platform9.net/keystone/v3/auth/tokens?nocatalog",
            method: "POST",
            headers: {},
            json: true,
            body: {
                auth: {
                    identity: {
                        methods: ["password"],
                        password: {
                            user: {
                                name: OPENSTACK_USERNAME,
                                domain: {
                                    id: "default"
                                },
                                password: OPENSTACK_PASSWORD
                            }
                        }
                    }
                }
            }
        };
        request(requestOptions, function(err, res, body) {
            var unscopedToken = res.headers['x-subject-token'];
            if (cb) {
                cb(unscopedToken);
            }
        });
    },

    getScopedToken: function(cb) {
        Openstack.getUnscopedToken(function(unscopedToken) {
            var requestOptions = {
                url: "https://klee-multi-glance.platform9.net/keystone/v3/auth/tokens?nocatalog",
                method: "POST",
                headers: {},
                json: true,
                body: {
                    auth: {
                        identity: {
                            methods: ["token"],
                            token: {
                                id: unscopedToken
                            }
                        },
                        scope: {
                            project: {
                                id: SERVICE_TENANT_ID
                            }
                        }
                    }
                }
            };
            request(requestOptions, function(err, res, body) {
                var scopedToken = res.headers['x-subject-token'];
                if (cb) {
                    cb(scopedToken);
                }
            });
        });
    },

    // This function will create a new tenant and a new user, and then
    // assign the admin role to the user on that tenant
    createNewUser: function(username, cb) {
        Openstack.getScopedToken(function(scopedToken) {
            var requestOptions = {
                url: "https://klee-multi-glance.platform9.net/keystone_admin/v2.0/tenants",
                method: "POST",
                headers: {'X-Auth-Token': scopedToken},
                json: true,
                body: {
                    tenant: {
                        enabled: true,
                        name: username,
                        description: "Tenant created for sandbox purposes. Will be deleted after 24 hours."
                    }
                }
            };
            request(requestOptions, function(err, res, body) {
                var tenantId = body.tenant.id;

                var requestOptions = {
                    url: "https://klee-multi-glance.platform9.net/keystone_admin/v2.0/users",
                    method: "POST",
                    headers: {'X-Auth-Token': scopedToken},
                    json: true,
                    body: {
                        user: {
                            name: username,
                            email: username,
                            password: "1Password!",
                            tenantId: tenantId
                        }
                    }
                };
                request(requestOptions, function(err, res, body) {
                    var userId = body.user.id;
                    
                    updateUser(username, userId, tenantId, function() {
                        var requestOptions = {
                            url: "https://klee-multi-glance.platform9.net/keystone_admin/v2.0/tenants/" + tenantId + "/users/" + userId + "/roles/OS-KSADM/" + ADMIN_ROLE_ID,
                            method: "PUT",
                            json: true,
                            headers: {'X-Auth-Token': scopedToken}
                        };
                        request(requestOptions, function(err, res, body) {
                            mail.sendMail(username, "1Password!", function(){
                                if (cb) {
                                    cb();
                                }
                            });
                        });
                    });
                });
            });
        });
    }
};

// Openstack.createNewUser("sandboxtest6@pf9.com");

module.exports = Openstack;
