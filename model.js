var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
    _id: {type: String},
    created: {type: String},
    tenant_id: {type: String},
    keystone_id: {type: String}
});

UserSchema.virtual('email').get(function() {
    return this._id;
});

var User = mongoose.model('User', UserSchema);

module.exports = {
    User: User
};
