var nodemailer = require("nodemailer");
var mandrill = require("mandrill-api/mandrill");

var mandrill_client = new mandrill.Mandrill('mowQvi5O9Nd7sfzIlHlBXw');

var mail = {
    sendMail: function(email, password, cb) {
        var message = {
            "html": "<p>Your sandbox account has been created!</p><p>Username: " + email +
            "</p><p>Password: " + password +
            "</p><p>You can access your account at <a href='https://klee-multi-glance.platform9.net'>https://klee-multi-glance.platform9.net</a>. This account will be active for 24 hours.</p>",
            "text": "Your sandbox account has been created, fill in the rest later",
            "subject": "Your Platform9 Sandbox Account is Ready",
            "from_email": "madhura@platform9.com",
            "from_name": "Madhura Maskasky",
            "to": [{
                "email": email,
                "type": "to"
            }],
            "headers": {
                "Reply-To": "support@platform9.com"
            }
        };
        mandrill_client.messages.send({"message": message, "async": false}, function(result) {
            if (cb) {
                cb();
            }
        }, function(err) {
            console.log(err);
            if (cb) {
                cb();
            }
        });
    }
};

module.exports = mail;
