var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/sandbox_database');

app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "http://localhost:4000");
    res.header('Access-Control-Allow-Credentials', "true");
    res.header('Access-Control-Allow-Methods', "HEAD,GET,PUT,POST,DELETE,OPTIONS,PATCH");
    res.header('Access-Control-Allow-Headers', "X-Auth-Token, Content-Type");
    next();
};

app.use(allowCrossDomain);
require('./api')(app);

app.listen(5555, function() {
    console.log("Listening on port 5555");
});
