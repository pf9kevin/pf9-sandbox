var Models = require("./model.js");
var User = Models.User;
var Openstack = require("./openstack.js");
var moment = require("moment");

module.exports = function(app){
    app.post('/user', function(req, res) {
        var email = req.body.email;
        User.find({_id: email}, function(err, users) {
            if (err) {
                res.status(500).send('Error finding users');
            }
            else {
                if (users.length > 0) {
                    res.status(409).send('User with that email already exists');
                }
                else {
                    var newUser = new User({
                        _id: email,
                        created: moment().utc().format()
                    });
                    newUser.save(function(err) {
                        if (err) {
                            res.status(500).send('Error creating user');
                        }
                        else {
                            // User created successfully
                            res.send(newUser);
                            Openstack.createNewUser(email);
                        }
                    });
                }
            }
        });
    });
};
